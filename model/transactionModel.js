const Sequelize = require('sequelize');

const sequelize = new Sequelize('postgres://postgres:abhi@127.0.0.1:5432/transaction');

sequelize
    .authenticate()
    .then(() => {
        console.log('Connection has been established successfully.');
    })
    .catch(err => {
        console.error('Unable to connect to the database:', err);
    });

const Op = Sequelize.Op;

const Transactions = sequelize.define('transaction', {
    id: {
        type: Sequelize.BIGINT,
        allowNull: false,
        primaryKey: true
    },
    amount: {
        type: Sequelize.DOUBLE,
        allowNull: false
    },
    type: {
        type: Sequelize.STRING,
        allowNull: false
    },
    parent_id: {
        type: Sequelize.BIGINT,
        allowNull: true
    }
}, {
    // options
});
Transactions.sync();

const ParentDetails = sequelize.define('parentTransactions', {
    id: {
        type: Sequelize.BIGINT,
        allowNull: false,
        primaryKey: true
    },
    amount: {
        type: Sequelize.DOUBLE,
        allowNull: false
    }
}, {
    // options
});
ParentDetails.sync();

const TypeDetails = sequelize.define('typeTransactions', {
    id: {
        type: Sequelize.BIGINT,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    type: {
        type: Sequelize.STRING,
        uniqueKey: true,
        allowNull: false
    },
    amount: {
        type: Sequelize.DOUBLE,
        allowNull: false
    }
}, {
    // options
});

TypeDetails.sync();


exports.createTransaction = async function (id, data) {
    data.id = id;

    //search if id exists
    let existingTransaction = await Transactions.findAll({where: {id: id}});


    let existingParent = false;
    if (data.parent_id) {
        existingParent = await ParentDetails.findAll({where: {id: data.parent_id}});
    }

    let existingType = await TypeDetails.findAll({where: {type: data.type}});

    if (existingTransaction && existingTransaction.length) {
        return Promise.reject({error: `Record already exist with if ${id}`})
    } else {
        await Transactions.create(data);
        await ParentDetails.create({id: id, amount: data.amount});

        if (data.parent_id && existingParent && existingParent.length) {
            await ParentDetails.update({
                    amount: sequelize.literal(`amount + ${data.amount}`),
                },
                {where: {id: data.parent_id}});
        } else if (data.parent_id && !(existingParent || existingParent.length)) {
            await ParentDetails.create({id: data.parent_id, amount: data.amount})
        }
        if (existingType && existingType.length) {
            await TypeDetails.update({
                    amount: sequelize.literal(`amount + ${data.amount}`),
                },
                {where: {type: data.type}});
        } else {
            await TypeDetails.create({type: data.type, amount: data.amount})
        }
        return Promise.resolve({message: `Data inserted successfully`, data: data});
    }


};

exports.fetchTransactionByType = async function (type) {
    let typeData = await TypeDetails.findAll({where: {type: type}});
    if (!typeData || !typeData.length) {
        return [0];
    } else {
        return [typeData[0].amount];
    }

};

exports.fetchTransactionSumById = async function (id) {
    let parentData = await ParentDetails.findAll({where: {id: id}});
    // console.log('parentData' , parentData);
    let data = parentData[0];
    console.log('data' , data);
    return {sum: data.amount};
    // let transactionData = await Transactions.findAll({where: {id: id}});
    //
    // if (!transactionData || !transactionData.length) {
    //     return Promise.reject({error: `No transaction with  ${id}`})
    // }
    // let data = transactionData[0];
    // if (data.parent_id) {
    //     let parentData = await ParentDetails.findAll({where: {id: data.parent_id}});
    //     if (parentData && parentData[0] && parentData[0].amount) {
    //         return {sum: data.amount + parentData[0].amount};
    //     } else {
    //
    //     }
    //     return {sum: data.amount};
    // } else {
    //     return {sum: data.amount};
    // }

};