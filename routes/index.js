const express = require('express');
const router = express.Router();
const TransactionModel = require('../model/transactionModel');

/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('Index', {title: 'Transaction Service'});
});

/* anity url for service watchers. */
router.get('/sanity', function (req, res, next) {
    res.send('UP');
    // res.send('UP', { title: 'Transaction Service' });
});

/* put transaction for creating a transaction */
router.put('/transaction/:transactionId', function (req, res, next) {
    let transactionId = parseInt(req.params.transactionId);
    let transactionBody = req.body;
    TransactionModel.createTransaction(transactionId, transactionBody)
        .then(transactionRes => {
            res.status(200).send(transactionRes);
        })
        .catch(err => {
            res.status(500).send(err);
        })
});

/* get total transaction by type */
router.get('/transaction/types/:type', function (req, res, next) {
    let transactionType = req.params.type;
    TransactionModel.fetchTransactionByType(transactionType)
        .then(transactionRes => {
            res.status(200).send(transactionRes);
        })
        .catch(err => {
            res.status(500).send(err);
        })
});

/* get sum of transaction for perticualar id. */
router.get('/transaction/sum/:transactionId', function (req, res, next) {
    let transactionId = parseInt(req.params.transactionId);
    TransactionModel.fetchTransactionSumById(transactionId)
        .then(transactionRes => {
            res.status(200).send(transactionRes);
        })
        .catch(err => {
            res.status(500).send(err);
        })
});


module.exports = router;
